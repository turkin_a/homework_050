class Machine {
  constructor(name) {
    if (name) this.name = name;
    else this.name = 'Machine';

    this.turned = false;
  }

  turnOn() {
    if (!this.turned) {
      this.turned = true;
      console.log(`${this.name} is turn on!`);
    }
  }

  turnOff() {
    if (this.turned) {
      this.turned = false;
      console.log(`${this.name} is turn off!`);
    }
  }
}

class HomeAppliance extends Machine {
  constructor(name) {
    if (name) super(name);
    else super('Appliance');

    this.plugging = false;
  }

  plugIn() {
    this.plugging = true;
    console.log(`${this.name} is plug in!`);
  }

  plugOff() {
    this.plugging = false;
    this.turnOff();
    console.log(`${this.name} is plug off!`);
  }

  turnOn() {
    if (this.plugging) super.turnOn();
    else console.log(`${this.name} is unplugged`);
  }
}

class WashingMachine extends HomeAppliance {
  constructor(name) {
    super(name);
    this.timer = 5000;
  }

  run(timer) {
    this.turnOn();
    if (!this.turned) {
      return;
    }

    if (timer >= 1 && timer <= 20) this.timer = timer * 1000;
    console.log(`${this.name} is running for ${this.timer / 1000} seconds`);

    setTimeout(() => {
      console.log(`${this.name} is stopping`);
      this.turnOff();
    }, this.timer);
  }
}

class LightSource extends HomeAppliance {
  constructor(name) {
    super(name);
    this.level = 50;
  }

  setLevel(level) {
    if (!this.turned) this.turnOn();

    if (!this.turned) {
      return;
    }

    if (level >= 1 && level <= 100) this.level = level;
    console.log(`${this.name} is on for ${this.level}%`);
  }
}

class AutoVehicle extends Machine {
  constructor(name) {
    super(name);
    this.x = 0;
    this.y = 0;
  }

  setPosition(x, y) {
    if (x >= 0 && x <= document.documentElement.clientWidth) this.x = x;
    else console.log(`Value "${x}" is wrong! X coordinate = ${this.x}`);

    if (y >= 0 && y <= document.documentElement.clientHeight) this.y = y;
    else console.log(`Value "${y}" is wrong! Y coordinate = ${this.y}`);

    if (this.icon) this.draw();
  }

  draw() {
    this.icon.css({'top': this.y, 'left': this.x});
    $('body').append(this.icon);
  }
}

class Car extends AutoVehicle {
  constructor(name) {
    if (name) super(name);
    else super('Car');

    this.speed = 10;
    this.icon = $('<i class="fa fa-car" aria-hidden="true"></i>')
      .css({'position': 'absolute', 'top': this.y, 'left': this.x})
      .append($('<span>')
        .css({'position': 'absolute', 'top': '16px', 'left': '0px', 'font-size': '8px'})
        .text(this.name));
  }

  hide() {
    this.icon.hide();
  }

  show() {
    this.icon.show();
  }

  setSpeed(speed) {
    if (speed > 0 && speed <= 200) this.speed = speed;
    else console.log(`Value "${speed}" is wrong! Speed = ${this.speed}`);
  }

  run(x, y) {
    this.turnOn();

    if (isNaN(x)) x = this.x;
    if (isNaN(y)) y = this.y;
    if (x === this.x && y === this.y) {
      console.log(`${this.name} stayed on the position`);
      return;
    }
    if (x > document.documentElement.clientWidth) x = document.documentElement.clientWidth;
    if (y > document.documentElement.clientHeight) y = document.documentElement.clientHeight;
    if (x < 0) x = 0;
    if (y < 0) y = 0;

    let directionX = true;
    let directionY = true;

    if (x < this.x) directionX = false;
    if (y < this.y) directionY = false;

    let currentX = this.x;
    let currentY = this.y;
    let corner = Math.abs(Math.atan((x - this.x) / (y - this.y)));
    let counter = 0;

    console.log(`${this.name} moving in ${x} : ${y} with speed ${this.speed}`);

    let interval = setInterval(() => {
      counter++;
      let finished = false;

      if (directionX) currentX += (this.speed * Math.sin(corner)) / 100;
      else currentX -= (this.speed * Math.sin(corner)) / 100;
      if (directionY) currentY += (this.speed * Math.cos(corner)) / 100;
      else currentY -= (this.speed * Math.cos(corner)) / 100;

      if (directionX && currentX > x || !directionX && currentX < x || directionY && currentY > y || !directionY && currentY < y) {
        clearInterval(interval);
        currentX = x;
        currentY = y;
        console.log(`${this.name} arrived in position ${x} : ${y}`);
        finished = true;
      }
      this.setPosition(currentX, currentY);
      if (counter % 100 === 0 && !finished) console.log(`${this.name} moving on ${Math.round(currentX)} : ${Math.round(currentY)}`);
    }, 10);
  }
}